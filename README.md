# Contenido
- [Configuración Habitual](#configuración-habitual)
  - [Clonar el Proyecto con Git](#clonar-el-proyecto-con-git)
  - [Instalación](#instalación)
  - [Entorno Virtual](#entorno-virtual)
  - [Instalar Paquetes de Python](#instalar-paquetes-de-python)
  - [Crear Nuevo Proyecto de Django](#crear-nuevo-proyecto-de-django)
  - [Correr el Servidor de Django](#correr-el-servidor-de-django)
  - [Configurar Docker](#configurar-docker)
  - [Comandos Docker](#comandos-docker)
- [Django Comparte Ride](#django-comparte-ride)
  - [NO Aplica Configuración Habitual](#no-aplica-configuración-habitual)
  - [Arquitectura de una Aplicación](#arquitectura-de-una-aplicación)
  - [The Twelve-Factor App](#the-twelve-factor-app)
  - [Codebase: Settings modular](#codebase-settings-modular)

# Configuración Habitual

## Clonar el Proyecto con Git
Clonamos el proyecto de Git

````bash
git clone git@gitlab.com:grupo-principal/inertia-sas/django-comparte-ride.git
````
Ingresamos a la carpeta del proyecto

````
cd django-comparte-ride
````

### Gitignore
Creamos el archivo `.gitignore`.

````
touch .gitignore
````
Dentro del archivo incluimos todas las carpeta y archivos que no deseamos almacenar en el control de versiones de `git`.

````
.venv
.vscode
**/__pycache__/
````

### Fuentes
- [Clone a repository with SSH](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-with-ssh)
- [Gitignore](https://git-scm.com/docs/gitignore)



## Instalación
### Instalación Gestor de Paquetes Homebrew para macOS
Instalamos el gestor de paquetes

````zsh
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
````
### Instalación Python 3, Gestor de Paquetes de Python y Entorno Virtual en macOS
Instalamos python 3.8 utilizando el Homebrew. Python ya tiene incluido el gestor de paquetes de python `pip3` y el entorno virtual `venv`.

````
brew install python@3.8
````

Si existen otras versiones de python instaladas, las desvinculamos.

````
brew unlink python@3.x
````

Vinculamos python 3.8

````
brew link --force python@3.8
````

### Instalación Python 3, Gestor de Paquetes de Python y Entorno Virtual en ubuntu 20.04
Actualizamos y refrezcamos las listas de repositorio

````
sudo apt-get update
````

Instalamos `python`.

````
sudo apt-get install python3.8
````

Instalamos el gestor de paquetes de python `pip`

````
sudo apt-get install python3-pip
````

Instalamos el entorno virtual `venv`

````
sudo apt-get install python3.8-venv
````

### Fuentes
- [Instala Homebrew en macOS](https://brew.sh/index_es)
- [Instala python@3.8 en macOS con Homebrew](https://formulae.brew.sh/formula/python@3.8)
- [Instala python3.8 en Ubuntu 20.04](https://ubuntu.pkgs.org/20.04/ubuntu-main-amd64/python3.8_3.8.2-1ubuntu1_amd64.deb.html)
- [Instala pip y venv en Ubuntu 20.04](https://packaging.python.org/guides/installing-using-linux-tools/#debian-ubuntu)



## Entorno Virtual
Creamos el entorno virtual

````
python3 -m venv .venv
````
Activamos el entorno virtual

````
source .venv/bin/activate

````

Desactivamos el entorno virtual

````
deactivate
````

### Fuentes
- [Entornos Virtuales y Paquetes](https://docs.python.org/es/3/tutorial/venv.html)
- [Creación de Entornos Virtuales](https://docs.python.org/es/3/library/venv.html#)



## Instalar Paquetes de Python
### Definicion de paquetes
Creamos el archivo `requirements.txt` que contendra los paquetes de python que intalaremos.

````
touch requirements.txt
````

Incluimos todos los paquetes de python que vamos a instalar.

````
Django
````
### Instalacion de paquetes
Instalamos los paquetes de python.

````
pip3 install -r requirements.txt
````

Verificamos todos los paquetes instalados.

````
pip3 freeze
````

Obteniendo como salida el paquete de Django intalado, al igual que todas sus dependencias.

````
asgiref==3.4.1
Django==3.2.8
pytz==2021.3
sqlparse==0.4.2
````

Las versiones de los paquetes instalados pueden cambiar, dependiendo de la version de Django instalada.

### Fuentes
- [Pip User Guide - Requirements Files](https://pip.pypa.io/en/latest/user_guide/#requirements-files)



## Crear Nuevo Proyecto de Django
Creamos la carpeta que contendra el proyecto de Django.

````
mkdir code
````
Ingresamos a la carpeta.

````
cd code
````
Creamos el proyecto de Django.

````
django-admin startproject webapp .
````

### Fuente
- [Django Documentation - Writing your first Django app - Creating a project](https://docs.djangoproject.com/en/3.2/intro/tutorial01/#creating-a-project)
- [Django Documentation - django-admin and manage.py - startproject](https://docs.djangoproject.com/en/3.2/ref/django-admin/#startproject)



## Correr el Servidor de Django
Si deseamos correr el proyecto dentro del equipo utilizamos

````
python3 manage.py runserver
````

Sin embargo, para este proyecto correremos el proyecto desde Docker.

### Fuente
- [Django Documentation - Writing your first Django app - The development server](https://docs.djangoproject.com/en/3.2/intro/tutorial01/#the-development-server)
- [Django Documentation - django-admin and manage.py - runserver](https://docs.djangoproject.com/en/3.2/ref/django-admin/#runserver)

## Configurar Docker
### Dockerfile

Creamos el `Dockerfile` necesario para construir la imagen de Docker.

````
touch Dockerfile
````

Incluimos las siguientes lineas dentro del `Dockerfile`

````dockerfile
FROM python:3.8
COPY ["requirements.txt","requirements.txt"]
RUN pip3 install -r requirements.txt
ENV WEBAPP_DIR=/code
WORKDIR $WEBAPP_DIR
COPY ["./code","."]
CMD ["python3","manage.py","runserver","0.0.0.0:8000"]
````

### Docker Compose

Creamos del archivo `docker-compose.yml` que creara el contenedor.

````
touch docker-compose.yml
````
Se incluyen las siguientes lineas dentro del  `docker-compose.yml`

````yml
version: '3.8'

services:
  web:
    build:
      context: .
    ports: 
      - 8000:8000
    volumes:
      - ./code:/code
````

### Dockerignore

Creamos el archivo `.dockerignore`

````
touch .dockerignore
````

Dentro del archivo incluimos todas las carpeta y archivos que no deseamos que se incluyan al construir la imagen de docker.

````
.venv
.vscode
.dockerignore
docker-compose.yml
Dockerfile
**/__pycache__/
````

### Fuentes
- [Dockerfile Reference](https://docs.docker.com/engine/reference/builder/)
- [Compose file version 3 reference](https://docs.docker.com/compose/compose-file/compose-file-v3/)
- [Dockerignore](https://docs.docker.com/engine/reference/builder/#dockerignore-file)



## Comandos Docker
### Crear imagen y contenedor

Creamos la imagen y el contenedor, y corremos el contenedor en background.

````
docker-compose up --build -d
````
### Conectarse nuevamente con el contenedor

Nos conectamos con los contenedores dependientes.

````
docker-compose up --attach-dependencies
````

### Ingresar al bash del contendor

Ingresamos al bash del contendor.

````
docker-compose exec web bash
````

### Consultar los logs

Generamos los logs del contenedor que esta corriendo, y nos mantenemos siguiendo las salida de los logs.

````
docker-compose logs -f
````

### Fuentes
- [Docker Compose CLI reference - docker-compose up](https://docs.docker.com/compose/reference/up/)
- [Docker Compose CLI reference - docker-compose exec](https://docs.docker.com/compose/reference/exec/)
- [Docker Compose CLI reference - docker-compose logs](https://docs.docker.com/compose/reference/logs/)



# Django Comparte Ride

## NO Aplica Configuración Habitual
Para este proyecto no utilizaremos la [Configuración Habitual](#configuración-habitual). Solamente aplicará lo correspondiente a la instalación, ya que los demás componentes tendrán una estrucutra de Docker y Django diferentes.

## Arquitectura de una Aplicación
El Backend consiste en:

- Servidor
- Aplicación
- Base de Datos

Un Backend developer es un diseñador, su trabajo consiste un 90% en leer, diseñar, analizar y planear. Un 10% en programar. Nuestro trabajo más importante es el diseño del sistema y las decisiones tomadas son más costosas y más difíciles de cambiar.

Web Services es la manera en que se implementan las arquitecturas orientadas a servicios, se crean bloques que son accesibles a través de la web, son independientes del lenguaje de programación.

- __*SOAP*__: Tiene su propio estándar, conocido por utilizar XML.
- __*REST*__: Representational State Transfer, el objetivo es que nuestras operaciones sean Stateless. REST depende mucho más del protocolo HTTP.
- __*GraphQL*__: Es el más moderno, desarrollado por Facebook. Funciona más como un Query Language para las API, un lenguaje de consultas.

### Fuentes
- [Arquitectura de una Aplicacion - Presentacion](https://docs.google.com/presentation/d/1HlT7niPOISnHjk6ldbmCvZdiOeKqlzLNxfsGY5499_E/edit#slide=id.g4d4bf94f45_0_93)
- [Developer Roadmap](https://github.com/kamranahmedse/developer-roadmap)
- [API SOAP - usps](https://www.usps.com/business/web-tools-apis/welcome.htm)
- [API RESTful HTTP - Facebook](https://developers.facebook.com/docs/graph-api/)
- [API GaphQL - GitHub](https://docs.github.com/en/graphql)



## The Twelve-Factor App
Algunos principios de Twelve Factor app

- Formas declarativas de configuración
- Un contrato claro con el OS
- Listas para lanzar
- Minimizar la diferencia entre entornos
- Fácil de escalar

__*Codebase*__: Se refiere a que nuestra app siempre debe estar trackeada por un sistema de control de versiones como Git, Mercurial, etc. Una sola fuente de verdad.

__*Dependencias*__: Una 12 factor app nunca debe depender de la existencia implícita de nuestro OS, siempre se declaran explícitamente qué dependencias usa el proyecto y se encarga de que estas no se filtren. Dependency Isolation.

__*Configuración*__: Acá nos referimos a algo que va a cambiar durante entornos.

__*Backing services*__: Estos pueden ser conectados y desconectados a voluntad. Es cualquier servicio que nuestra aplicación puede consumir a través de la red como Base de Datos, Mensajería y Cola, Envío de Emails o Caché.

__*Build, release, run*__: Separa estrictamente las etapas de construcción y las de ejecución. Build es convertir nuestro código fuente en un paquete.Release es la etapa donde agregamos a nuestro paquete cosas de configuración como variables de entorno y Run donde corremos la aplicación en el entorno correspondiente. Las etapas no se pueden mezclar.

__*Procesos*__: En el caso más complejo tenemos muchos procesos corriendo como Celery y Redis, en esta parte los procesos son stateless y no comparten nada. Cualquier dato que necesite persistir en memoria o en disco duro tiene que almacenarse en un backing services,

__*Dev/prod parity*__: Reducir la diferencia entre entornos para reducir tiempo entre deploys y las personas involucradas sean las mismas que puedan hacer el deploy

__*Admin processes*__: Tratar los procesos administrativos como una cosa diferente, no deben estar con la app.

### Fuentes
- [The Twelve-Factor App](https://12factor.net/es/)



## Codebase: Settings modular

__Objetivos__:
- Configuración _declarativa_.
- _Contrato claro_ con el OS.
- Lista para _lanzar_.
- _Reducir diferencia_ entre entornos.
- _Fácil de escalar_.

__Docker__ ayuda cumplir estos objetivos:
- No necesita un "Guest OS".
- Usa muy poca memoria.
- Fácil de replicar y controlar.
- Fácil de compartir. 

__Servicio de Docker del proyecto__:
- Django (port: 8000)
- PostgreSQL (port: 5432)
- Redis (port: 6379)
- Celery (port: 5555). Tiene tres servicios.
  - Brocker
  - Bit
  - Flower: Es una interfaz visual que permite ver que está sucediendo en tiempo real con Celery. Es el que realmente está corriendo en el puerto 5555.

### Docker Compose
  
````
docker-compose -f local.yml build
````

### Fuentes
- [Docker](https://docs.docker.com/get-docker/)
- [Docker Compose](https://docs.docker.com/compose/install/)

## Config
Creamos las carpetas requeridas.

````
mkdir config && mkdir 'config/settings'
````
Creamos los archivos requeridos.

````
touch 'config/__init__.py' 'config/urls.py' 'config/wsgi.py' 'config/settings/_init__.py' 'config/settings/base.py' 'config/settings/local.py' 'config/settings/production.py' 'config/settings/test.py'
````
Obteniendo la siguiente estructura:
````
config
├── __init__.py
├── settings
│   ├── base.py
│   ├── _init__.py
│   ├── local.py
│   ├── production.py
│   └── test.py
├── urls.py
└── wsgi.py

````
### config/wsgi.py
````py
"""
WSGI config for Comparte Ride project.

This module contains the WSGI application used by Django's development server
and any production WSGI deployments. It should expose a module-level variable
named ``application``. Django's ``runserver`` and ``runfcgi`` commands discover
this application via the ``WSGI_APPLICATION`` setting.

Usually you will have the standard Django WSGI application here, but it also
might make sense to replace the whole Django WSGI application with a custom one
that later delegates to the Django one. For example, you could introduce WSGI
middleware here, or combine a Django application with an application of another
framework.

"""
import os
import sys

from django.core.wsgi import get_wsgi_application

# This allows easy placement of apps within the interior
# cride directory.
app_path = os.path.abspath(os.path.join(
    os.path.dirname(os.path.abspath(__file__)), os.pardir))
sys.path.append(os.path.join(app_path, 'cride'))

# We defer to a DJANGO_SETTINGS_MODULE already in the environment. This breaks
# if running multiple sites in the same mod_wsgi process. To fix this, use
# mod_wsgi daemon mode with each site in its own daemon process, or use
# os.environ["DJANGO_SETTINGS_MODULE"] = "config.settings.production"
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.production")

# This application object is used by any WSGI server configured to use this
# file. This includes Django's development server, if the WSGI_APPLICATION
# setting points here.
application = get_wsgi_application()
````
### config/urls.py
````py
"""Main URLs module."""

from django.conf import settings
from django.urls import path
from django.conf.urls.static import static
from django.contrib import admin

urlpatterns = [
    # Django Admin
    path(settings.ADMIN_URL, admin.site.urls),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
````

### config/settings/base.py
````py
"""Base settings to build other settings files upon."""

import environ

ROOT_DIR = environ.Path(__file__) - 3
APPS_DIR = ROOT_DIR.path('cride')

env = environ.Env()

# Base
DEBUG = env.bool('DJANGO_DEBUG', False)

# Language and timezone
TIME_ZONE = 'America/Mexico_City'
LANGUAGE_CODE = 'en-us'
SITE_ID = 1
USE_I18N = True
USE_L10N = True
USE_TZ = True

# DATABASES
DATABASES = {
    'default': env.db('DATABASE_URL'),
}
DATABASES['default']['ATOMIC_REQUESTS'] = True

# URLs
ROOT_URLCONF = 'config.urls'

# WSGI
WSGI_APPLICATION = 'config.wsgi.application'

# Apps
DJANGO_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
]

THIRD_PARTY_APPS = [
]
LOCAL_APPS = [
]
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

# Passwords
PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.Argon2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
]
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Middlewares
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

# Static files
STATIC_ROOT = str(ROOT_DIR('staticfiles'))
STATIC_URL = '/static/'
STATICFILES_DIRS = [
    str(APPS_DIR.path('static')),
]
STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

# Media
MEDIA_ROOT = str(APPS_DIR('media'))
MEDIA_URL = '/media/'

# Templates
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            str(APPS_DIR.path('templates')),
        ],
        'OPTIONS': {
            'debug': DEBUG,
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# Security
SESSION_COOKIE_HTTPONLY = True
CSRF_COOKIE_HTTPONLY = True
SECURE_BROWSER_XSS_FILTER = True
X_FRAME_OPTIONS = 'DENY'

# Email
EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND', default='django.core.mail.backends.smtp.EmailBackend')

# Admin
ADMIN_URL = 'admin/'
ADMINS = [
    ("""Luis Camilo Jimenez""", 'luisca1985@gmail.com'),
]
MANAGERS = ADMINS

# Celery
INSTALLED_APPS += ['cride.taskapp.celery.CeleryAppConfig']
if USE_TZ:
    CELERY_TIMEZONE = TIME_ZONE
CELERY_BROKER_URL = env('CELERY_BROKER_URL')
CELERY_RESULT_BACKEND = CELERY_BROKER_URL
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERYD_TASK_TIME_LIMIT = 5 * 60
CELERYD_TASK_SOFT_TIME_LIMIT = 60
````
### config/settings/local.py
````py
"""Development settings."""

from .base import *  # NOQA
from .base import env

# Base
DEBUG = True

# Security
SECRET_KEY = env('DJANGO_SECRET_KEY', default='PB3aGvTmCkzaLGRAxDc3aMayKTPTDd5usT8gw4pCmKOk5AlJjh12pTrnNgQyOHCH')
ALLOWED_HOSTS = [
    "localhost",
    "0.0.0.0",
    "127.0.0.1",
]

# Cache
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': ''
    }
}

# Templates
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG  # NOQA

# Email
EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND', default='django.core.mail.backends.console.EmailBackend')
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025

# django-extensions
INSTALLED_APPS += ['django_extensions']  # noqa F405

# Celery
CELERY_TASK_ALWAYS_EAGER = True
CELERY_TASK_EAGER_PROPAGATES = True
````
### config/settings/production.py
````py
"""Production settings."""

from .base import *  # NOQA
from .base import env

# Base
SECRET_KEY = env('DJANGO_SECRET_KEY')
ALLOWED_HOSTS = env.list('DJANGO_ALLOWED_HOSTS', default=['comparteride.com'])

# Databases
DATABASES['default'] = env.db('DATABASE_URL')  # NOQA
DATABASES['default']['ATOMIC_REQUESTS'] = True  # NOQA
DATABASES['default']['CONN_MAX_AGE'] = env.int('CONN_MAX_AGE', default=60)  # NOQA

# Cache
CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': env('REDIS_URL'),
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
            'IGNORE_EXCEPTIONS': True,
        }
    }
}

# Security
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SECURE_SSL_REDIRECT = env.bool('DJANGO_SECURE_SSL_REDIRECT', default=True)
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True
SECURE_HSTS_SECONDS = 60
SECURE_HSTS_INCLUDE_SUBDOMAINS = env.bool('DJANGO_SECURE_HSTS_INCLUDE_SUBDOMAINS', default=True)
SECURE_HSTS_PRELOAD = env.bool('DJANGO_SECURE_HSTS_PRELOAD', default=True)
SECURE_CONTENT_TYPE_NOSNIFF = env.bool('DJANGO_SECURE_CONTENT_TYPE_NOSNIFF', default=True)

# Storages
INSTALLED_APPS += ['storages']  # noqa F405
AWS_ACCESS_KEY_ID = env('DJANGO_AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = env('DJANGO_AWS_SECRET_ACCESS_KEY')
AWS_STORAGE_BUCKET_NAME = env('DJANGO_AWS_STORAGE_BUCKET_NAME')
AWS_QUERYSTRING_AUTH = False
_AWS_EXPIRY = 60 * 60 * 24 * 7
AWS_S3_OBJECT_PARAMETERS = {
    'CacheControl': f'max-age={_AWS_EXPIRY}, s-maxage={_AWS_EXPIRY}, must-revalidate',
}

# Static  files
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

# Media
DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
MEDIA_URL = f'https://{AWS_STORAGE_BUCKET_NAME}.s3.amazonaws.com/'

# Templates
TEMPLATES[0]['OPTIONS']['loaders'] = [  # noqa F405
    (
        'django.template.loaders.cached.Loader',
        [
            'django.template.loaders.filesystem.Loader',
            'django.template.loaders.app_directories.Loader',
        ]
    ),
]

# Email
DEFAULT_FROM_EMAIL = env(
    'DJANGO_DEFAULT_FROM_EMAIL',
    default='Comparte Ride <noreply@comparteride.com>'
)
SERVER_EMAIL = env('DJANGO_SERVER_EMAIL', default=DEFAULT_FROM_EMAIL)
EMAIL_SUBJECT_PREFIX = env('DJANGO_EMAIL_SUBJECT_PREFIX', default='[Comparte Ride]')

# Admin
ADMIN_URL = env('DJANGO_ADMIN_URL')

# Anymail (Mailgun)
INSTALLED_APPS += ['anymail']  # noqa F405
EMAIL_BACKEND = 'anymail.backends.mailgun.EmailBackend'
ANYMAIL = {
    'MAILGUN_API_KEY': env('MAILGUN_API_KEY'),
    'MAILGUN_SENDER_DOMAIN': env('MAILGUN_DOMAIN')
}

# Gunicorn
INSTALLED_APPS += ['gunicorn']  # noqa F405

# WhiteNoise
MIDDLEWARE.insert(1, 'whitenoise.middleware.WhiteNoiseMiddleware')  # noqa F405


# Logging
# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See https://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s '
                      '%(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True
        },
        'django.security.DisallowedHost': {
            'level': 'ERROR',
            'handlers': ['console', 'mail_admins'],
            'propagate': True
        }
    }
}

````
### config/settings/test.py
````py
"""Testing settings.

With these settings, tests run faster.
"""

from .base import *  # NOQA
from .base import env

# Base
DEBUG = False
SECRET_KEY = env("DJANGO_SECRET_KEY", default="7lEaACt4wsCj8JbXYgQLf4BmdG5QbuHTMYUGir2Gc1GHqqb2Pv8w9iXwwlIIviI2")
TEST_RUNNER = "django.test.runner.DiscoverRunner"

# Cache
CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.locmem.LocMemCache",
        "LOCATION": ""
    }
}

# Passwords
PASSWORD_HASHERS = ["django.contrib.auth.hashers.MD5PasswordHasher"]

# Templates
TEMPLATES[0]["OPTIONS"]["debug"] = DEBUG  # NOQA
TEMPLATES[0]["OPTIONS"]["loaders"] = [  # NOQA
    (
        "django.template.loaders.cached.Loader",
        [
            "django.template.loaders.filesystem.Loader",
            "django.template.loaders.app_directories.Loader",
        ],
    )
]

# Email
EMAIL_BACKEND = "django.core.mail.backends.locmem.EmailBackend"
EMAIL_HOST = "localhost"
EMAIL_PORT = 1025
````

### Fuentes
- [Configuring Django Settings: Best Practices](https://djangostars.com/blog/configuring-django-settings-best-practices/)
- [django-environ - paquete de python que permite usar Twelve-factor methodology`](https://django-environ.readthedocs.io/en/latest/)
- [Two Scoops of Django 3.x](https://www.feldroy.com/books/two-scoops-of-django-3-x)
- [Setting Up a Secure Django Project Repository With Docker and Django-Environ](https://medium.com/swlh/setting-up-a-secure-django-project-repository-with-docker-and-django-environ-4af72ce037f0)
- [Django Extensions](https://github.com/django-extensions/django-extensions)
- [Anymail: Django email integration for transactional ESPs](https://github.com/anymail/django-anymail)

## Requirements
Creamos las carpeta `requirements`.

````
mkdir requirements
````
Creamos los archivos requeridos.

````
touch 'requirements/base.txt' 'requirements/local.txt' 'requirements/production.txt'
````
Obteniendo la siguiente estructura:
````
requirements
├── base.txt
├── local.txt
└── production.txt

````

### requirements/base.txt
````py
# Base
pytz==2018.7
python-slugify==1.2.6
Pillow==5.3.0
psycopg2==2.7.4 --no-binary psycopg2

# Django
django==2.0.9

# Environment
django-environ==0.4.5

# Passwords security
argon2-cffi==18.3.0

# Static files
whitenoise==4.1.2

# Celery
redis>=2.10.6, < 3
django-redis==4.10.0
celery==4.2.1
flower==0.9.2
````

### requirements/local.txt
````py
-r ./base.txt

# Debugging
ipdb==0.11

# Tools
django-extensions==2.1.4

# Testing
mypy==0.650
pytest==4.0.2
pytest-sugar==0.9.2
pytest-django==3.4.4
factory-boy==2.11.1

# Code quality
flake8==3.6.0
````
### requirements/production.txt
````py
# PRECAUTION: avoid production dependencies that aren't in development

-r ./base.txt

gunicorn==19.9.0

# Static files
django-storages[boto3]==1.7.1

# Email
django-anymail[mailgun]==5.0
````
### Fuentes
- [django-storages](https://github.com/jschneier/django-storages)
- [django-anymail](https://github.com/anymail/django-anymail)
- [django-extensions](https://github.com/django-extensions/django-extensions)
- [django-environ](https://github.com/joke2k/django-environ)
- [sentry](https://github.com/getsentry/sentry)